public class ovningar {
    public static void main(String[] args) {

        //1 Detta är ett objekt av klassen Human,
        //Human klassen är en mall för hur objektet ser ut
        Human human = new Human("Anton");
    }
}
//3 Utskriften kommer att bli 3 och det är pga att man anropar metoden init som
//initierar count med 1 sedan när man anropar metoden increment så adderas 1 till det tidigare
//värdet i count vilket resulterar i
//count + 1 = 2 vilket gör att count blir till 2 och sedan så anropar man metoden increment ännu en gång
//vilket adderar 1 igen till count vilket resulterar i count = 2 + 1 = 3
//sedan skriver man ut count vilket är 3.

