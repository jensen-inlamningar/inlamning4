public class Human {

    String name;

    //2 klassvaribaler är statiska variabler som är deklararerade inuti klassen
    //dessa variabler kan bara ha en kopia utanför sin klass
    static String name1 = "Sofia";
    public Human(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
